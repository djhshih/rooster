library(io);
library(lpSolve);
library(argparser);

pr <- arg_parser("Rostering program that makes you say 'oo' instead of 'aw'");
pr <- add_argument(pr, "--min_females", "minimum number of female fielders", 3);
pr <- add_argument(pr, "--weights",
	"weights for fielding ability, preference, and importance");
pr <- add_argument(pr, "--ability", "player fielding ability file");
pr <- add_argument(pr, "--preference", "player fielding preference file");
pr <- add_argument(pr, "--importance", "fielding position importance file");
pr <- add_argument(pr, "--attendance", "attendance list file");
pr <- add_argument(pr, "players", "players information file");
pr <- add_argument(pr, "flineup", "fielding lineup output file");

#argv <- parse_args(pr);
argv <- parse_args(pr, c(
	"--ability", "fielding.tsv",
	"--preference", "preference.tsv",
	"--importance", "importance.tsv",
	"--attendance", "attendance.txt",
	"players.tsv",
	"fielding_lineup.mtx"
));

min_females <- argv$min_females;
if (is.na(argv$weights)) {
	weights <- c(1, 0.1, 1)
} else {
	weights <- argv$weights;
}

x <- qread(argv$players);
fielding <- qread(argv$ability);
preference <- qread(argv$preference);
importance <- qread(argv$importance);
attendance <- readLines(argv$attendance);

x <- x[x$player %in% attendance, ];

fielding <- fielding[match(x$player, fielding$player), ];
preference <- preference[match(x$player, preference$player), ];


multrows <- function(A, b) {
	t(t(A) * b)
} 

addrows <- function(A, b) {
	t(t(A) + b)
} 

# factor in fielding performances, preferences, and importances
tmp <- weights[1] * as.matrix(fielding[, 2:ncol(fielding)]) +
	weights[2] * as.matrix(preference[, 2:ncol(preference)]);
S <- addrows(tmp, weights[3] * importance$score);
rownames(S) <- fielding$player;


nplayers <- nrow(S);
npos <- ncol(S);

tscores_player <- apply(S, 1, sum);


# allocate players to innings
# need at least 3 females each inning
# assume there are more than 3 females

# assume 7 innings and 9 fielders per inning, totalling 63 fielding slots

# 13 players - 9 fielders  =>  4 players sit per inning, totalling 28 sitting slots
# 28/13 > 2  =>  each player sits at least 2 innings
# 2 * 13 players = 26 sits
# 28 - 26 = 2 sits remaining
# therefore, the 2 subs will sit an extra inning

nextras <- nplayers - npos;
stopifnot(nextras >= 0);

ninnings <- 7;

nsits <- nextras * ninnings;

nplayersits_pad <- 1;
mean_nplayersits <- nsits / nplayers;
max_nplayersits <- floor(mean_nplayersits + nplayersits_pad);
min_nplayersits <- floor(mean_nplayersits);


check_fielding <- function(x) {
	valid <- TRUE;
	row_sums <- apply(x, 1, sum);
	valid <- valid && all(row_sums <= 1);
	
	col_sums <- apply(x, 2, sum);
	valid <- valid && all(col_sums == 1);

	valid
}

score_fielding <- function(F, S) {
	sum(F * S)
}

repmat <- function(x, m=1, n=1) {
	y <- NULL;
	for (i in 1:m) {
		y <- rbind(y, x);
	}
	x <- y;
	y <- NULL;
	for (i in 1:n) {
		y <- cbind(y, x);
	}
	y
}

# create block diagonal matrix by replicating x
repblock <- function(x, n) {
	mx <- nrow(x);
	nx <- ncol(x);
	y <- matrix(0, nrow=mx*n, ncol=nx*n);
	for (i in 1:n) {
		y[(1:mx) + (i-1)*mx, (1:nx) + (i-1)*nx] <- x;
	}
	y
}

nelem <- function(x) {
	prod(dim(x))
}

# linear programming with constraints on f

# number of variables in a block
nvars <- nelem(S);

# S is nplayer x npos
objf.coef <- rep(c(S), ninnings);

nvarst <- length(objf.coef);

# ensure that each column sum of S == 1
# (exactly one fielder at each position)

# create constraint matrix for one block (inning)
const1 <- matrix(0, nrow=npos, ncol=nvars);
for (i in 1:npos) {
	const1[i, (1:nplayers) + (i-1)*nplayers] <- 1;
}
dir1 <- rep("==", npos);
rhs1 <- rep(1, npos);

const1_all <- repblock(const1, ninnings);
dir1_all <- rep(dir1, ninnings);
rhs1_all <- rep(rhs1, ninnings);

# ensure that each row sum of S <= 1
# (each fielder plays at most one position)

# create constraint matrix for one block (inning)
const2 <- matrix(0, nrow=nplayers, ncol=nvars);
for (i in 1:nplayers) {
	const2[i, seq(i, nvars, nplayers)] <- 1;
}

dir2 <- rep("<=", nplayers);
rhs2 <- rep(1, nplayers);

const2_all <- repblock(const2, ninnings);
dir2_all <- rep(dir2, ninnings);
rhs2_all <- rep(rhs2, ninnings);

# ensure that no player sits more than max_nplayersits
# in other words, every player plays at least ninnings - max_nplayersits innings
const3_all <- matrix(0, nrow=nplayers, ncol=nvarst);
for (i in 1:nplayers) {
	const3_all[i, seq(i, nvarst, nplayers)] <- 1;
}
dir3_all <- rep(">=", nplayers);
rhs3_all <- rep(ninnings - max_nplayersits, nplayers);

# ensure that the minimum female fielder per inning requirement is met
const4_all <- matrix(0, nrow=ninnings, ncol=nvarst);
for (i in 1:ninnings) {
	const4 <- array(0, dim=c(nplayers, npos, ninnings));
	const4[x$gender == "F", , i] <- 1;
	const4_all[i, ] <- const4;
}
dir4_all <- rep(">=", ninnings);
rhs4_all <- rep(min_females, ninnings);

# ensure that no player sits less than min_nplayersits
# in other words, every player plays at most ninnings - min_nplayersits innings
const5_all <- matrix(0, nrow=nplayers, ncol=nvarst);
for (i in 1:nplayers) {
	const5_all[i, seq(i, nvarst, nplayers)] <- 1;
}
dir5_all <- rep("<=", nplayers);
rhs5_all <- rep(ninnings - min_nplayersits, nplayers);


s <- lp(
	direction = "max",
	objective.in = objf.coef,
	const.mat = rbind(const1_all, const2_all, const3_all, const4_all, const5_all), 
	const.dir = c(dir1_all, dir2_all, dir3_all, dir4_all, dir5_all),
	const.rhs = c(rhs1_all, rhs2_all, rhs3_all, rhs4_all, rhs5_all),
	all.bin = TRUE,
	num.bin.solns = 1
);

Gs <- list();
nn <- length(S);
for (i in 1:ninnings) {
	Gs[[i]] <- matrix(s$solution[1:nn + (i - 1)*nn], nrow=nplayers);
}

unlist(lapply(Gs, check_fielding));

count_fieldings <- function(F) {
	apply(F, 1, sum)
}

# table of whether a player fields each inning
fields <- matrix(unlist(lapply(Gs, count_fieldings)), nrow=nplayers);
rownames(fields) <- rownames(S);


fielding_to_lineup <- function(F, S, abbrev=TRUE) {
	rn <- rownames(S);

	if (abbrev) {
		rn <- gsub("(.+) (.).*", "\\1 \\2", rn);
	}

	cn <- colnames(S);
	lineup <- rn[apply(F == 1, 2, which)];
	names(lineup) <- colnames(S);

	# fill in cheerleaders
	off <- rn[apply(F, 1, sum) == 0];
	names(off) <- rep("CL", length(off));
	
	c(lineup, off)
}



inning_distance <- function(x, y) {
	(sum(x == 0 & y == 0) * 1.0)
	#(sum(x == 0 & y == 0) * 1.0) + (sum(x == 1 & y == 1) * 0.1)
}

inning_distance_matrix <- function(X) {
	Y <- matrix(0, nrow=ncol(X), ncol=ncol(X));
	for (i in 1:ncol(X)) {
		for (j in i:ncol(X)) {
			Y[i, j] <- Y[j, i] <- inning_distance(X[,i], X[,j]);	
		}
	}
	Y
}

d <- inning_distance_matrix(fields);

# select inning order by greedily select pair that has minimal conflict
# NOTE: this is not optimum
inning_rank <- integer(ninnings);
ranked <- order(apply(d, 2, function(z) sum(z == 0)));
k <- ranked[1]
for (i in 1:ninnings) {
	inning_rank[k] <- i;
	if (i == ninnings) break;
	idx <- inning_rank == 0;
	min.idx <- which.min(d[, k][idx]);
	k <- match(min.idx, cumsum(idx));
}

inning_order <- order(inning_rank);

Gs.ordered <- Gs[inning_order];

fields.ordered <- fields[, inning_order]

flineup.list <- lapply(Gs.ordered, FUN=fielding_to_lineup, S=S);
flineup <- matrix(unlist(flineup.list), ncol=ninnings);
rownames(flineup) <- names(flineup.list[[1]]);
colnames(flineup) <- 1:ninnings;

npfields <- apply(fields.ordered, 1, sum)

qwrite(flineup, argv$flineup);

