library(io);
library(lpSolve);

x <- qread("fielding.tsv");
attendance <- readLines("attendance.txt");
x <- x[x$player %in% attendance, ];

S <- as.matrix(x[, 4:ncol(x)]);
rownames(S) <- x[,1];

nplayers <- nrow(S);
npos <- ncol(S);

tscores_player <- apply(S, 1, sum);


# allocate players to innings
# need at least 3 females each inning
# assume there are more than 3 females

# assume 7 innings and 9 fielders per inning, totalling 63 fielding slots

# 13 players - 9 fielders  =>  4 players sit per inning, totalling 28 sitting slots
# 28/13 > 2  =>  each player sits at least 2 innings
# 2 * 13 players = 26 sits
# 28 - 26 = 2 sits remaining
# therefore, the 2 subs will sit an extra inning

nextras <- nplayers - npos;
stopifnot(nextras >= 0);

ninnings <- 7;

nsits <- nextras * ninnings;

#min_nplayersits <- floor(nsits / nplayers);

sit_rank <- order(x$starting, tscores_player);
x[sit_rank, ]


# allocate sits
# sits for each player
npsits <- numeric(nplayers);
nsits_remaining <- nsits;
i <- 1;
while (nsits_remaining > 0) {
	npsits[sit_rank[i]] <- npsits[sit_rank[i]] + 1;
	i <- i + 1;
	if (i > nplayers) i <- 1;
	nsits_remaining <- nsits_remaining - 1;
}


# determine which players sit together
# constraint: minimum of 3 female fielders
# objective: maximize the minimum total score of fielders in any inning
min_females <- 3;
sits <- lapply(1:nplayers, function(i) list());

# 6 female players => maximum of 6 players - 3 fielders = 3 female players sit per inning
nfemales <- sum(x$gender == "F");
max_female_sit <- nfemales - min_females;
stopifnot(max_female_sit > 0);




f <- sample(1:nplayers, npos);
F <- matrix(0, nrow=nplayers, ncol=npos);
for (i in 1:npos) {
	F[f[i], i] <- 1;
}

check_fielding <- function(x) {
	valid <- TRUE;
	row_sums <- apply(x, 1, sum);
	valid <- valid && all(row_sums <= 1);
	
	col_sums <- apply(x, 2, sum);
	valid <- valid && all(col_sums == 1);

	valid
}

check_fielding(F);

score_fielding <- function(F, S) {
	sum(F * S)
}

score_fielding(F, S);

# linear programming with constraints on f

# S is nplayer x npos
objf.coef <- c(S);

# ensure that each column sum of S == 1
const1 <- matrix(0, nrow=npos, ncol=length(objf.coef));
for (i in 1:npos) {
	const1[i, (1:nplayers) + (i-1)*nplayers] <- 1;
}
dir1 <- rep("==", npos);
rhs1 <- rep(1, npos);

# ensure that each row sum of S <= 1
const2 <- matrix(0, nrow=nplayers, ncol=length(objf.coef));
for (i in 1:nplayers) {
	const2[i, seq(i, length(objf.coef), nplayers)] <- 1;
}
dir2 <- rep("<=", nplayers);
rhs2 <- rep(1, nplayers);

s <- lp(
	direction = "max",
	objective.in = objf.coef,
	const.mat = rbind(const1, const2), 
	const.dir = c(dir1, dir2),
	const.rhs = c(rhs1, rhs2),
	all.bin = TRUE,
	num.bin.solns = 10
);

Gs <- list();
nn <- length(S);
for (i in 1:s$num.bin.solns) {
	Gs[[i]] <- matrix(s$solution[1:nn + (i - 1)*nn], nrow=nplayers);
}

#G <- matrix(s$solution, nrow=nplayers);
#check_fielding(G);

fielding_to_lineup <- function(F, S) {
	rn <- rownames(S);
	cn <- colnames(S);
	lineup <- rn[apply(F == 1, 2, which)];
	names(lineup) <- colnames(S);
	lineup
}

lapply(Gs, FUN=fielding_to_lineup, S=S);


